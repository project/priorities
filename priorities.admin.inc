<?php

/**
 * @file
 * Administrative page callbacks for the priorities module.
 */

/**
 * Menu callback; returns a listing of all defined surveys.
 */
function priorities_admin_overview() {
  $header = array(
    array('data' => t('Title'), 'field' => 'title', 'sort' => 'asc'),
    array('data' => t('Status'), 'field' => 'status'),
    array('data' => t('Description'), 'field' => 'description'),
    array('data' => t('Start date'), 'field' => 'starttime'),
    array('data' => t('Duration (in seconds)'), 'field' => 'duration'),
    array('data' => t('Operations'), 'colspan' => '2')
  );

  $rows = array();
  $destination = drupal_get_destination();
  $surveys = priorities_get_surveys();
  foreach ($surveys as $survey_name => $survey) {
    // We want to show only surveys where user has get or set access rights.
    if (priorities_get_access($survey) || priorities_set_access($survey)) {
      $row = array(
        'data' => array(
          $survey['title'],
          priorities_survey_status($survey['status']),
          check_markup($survey['description'], $survey['format'], FALSE),
          date('Y-m-d H:i:s', $survey['starttime']),
          $survey['duration'],
          l(t('edit'), "admin/config/priorities/edit/$survey_name", array('query' => $destination)),
          l(t('delete'), "admin/config/priorities/delete/$survey_name", array('query' => $destination)),
        ),
      );
      $rows[] = $row;
    }
  }

  $build['priorities_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No survey has been defined yet. <a href="@link">Add a survey</a>.', array('@link' => url('admin/config/priorities/add'))),
  );
  $build['priorities_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Menu callback; handles pages for creating and editing surveys.
 */
function priorities_admin_edit($survey = array()) {
  if ($survey) {
    drupal_set_title($survey['survey_name']);
    $output = drupal_get_form('priorities_admin_form', $survey);
  }
  else {
    $output = drupal_get_form('priorities_admin_form');
  }

  return $output;
}

/**
 * Form builder for adding/editing a survey.
 *
 * @param $survey
 *   A survey array returned by priorities_survey_load().
 *
 * @see priorities_admin_form_delete_submit()
 * @see priorities_admin_form_validate()
 * @see priorities_admin_form_submit()
 * @ingroup forms
 */
function priorities_admin_form($form, &$form_state, $survey = array('survey_name' => '', 'title' => '', 'description' => '', 'sorted' => 1, 'status' => 1, 'showbefore' => 1, 'modify' => 1, 'anonymous' => 0, 'starttime' => 0, 'duration' => 0, 'display' => 0, 'nodetype' => 'priorities_alternative', 't_name' => '', 't_description' => '')) {
  $form['actions'] = array('#type' => 'actions', '#weight' => 20,);
  drupal_add_css(drupal_get_path('module', 'priorities') . '/priorities.css');
  if ($survey['survey_name']) {
    $form['survey_name'] = array('#type' => 'value', '#value' => $survey['survey_name']);
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('priorities_admin_form_delete_submit'),
      '#weight' => 2,
    );
    $form['#insert'] = FALSE;
  }
  else {
    $form['survey_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Survey name'),
      '#maxsize' => PRIORITIES_MAX_SURVEY_NAME_LENGTH_UI,
      '#description' => t('The machine-readable name of this survey. This text will be used for constructing the URL of the <em>priorities overview</em> page for this survey. This name must contain only lowercase letters, numbers, and hyphens, and must be unique.'),
      '#required' => TRUE,
    );
    $form['#insert'] = TRUE;
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $survey['title'],
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'text_format',
    '#base_type' => 'textarea',
    '#rows' => 5,
    '#title' => t('Description'),
    '#default_value' => $survey['description'],
    '#format' => isset($survey['format']) ? $survey['format'] : NULL,
    '#required' => FALSE,
  );
  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#default_value' => $survey['status'],
    '#required' => FALSE,
    '#options' => priorities_survey_status(),
    '#description' => t('When a survey is <em>public</em>, authorized users can answer to the survey and see its on-going results. When a survey is  <em>active</em>, authorized users can only answer the survey. When a survey is <em>closed</em>, authorized users can only see its results. When a survey is <em>disabled</em>, users cannot answer the survey, nor see its results.'), 
  );
  $form['showbefore'] = array(
    '#type' => 'radios',
    '#title' => t('Show results before voting'),
    '#default_value' => isset($survey['showbefore']) ? $survey['showbefore'] : 1,
    '#required' => FALSE,
    '#options' => priorities_survey_showbefore(),
    '#description' => t('Allow or disallow users to see the results of the survey before voting (<em>this setting works only when the status of the survey is set to public</em>). This setting can be <em>bypassed</em> by a role based permission.')
  );
  $form['modify'] = array(
    '#type' => 'radios',
    '#title' => t('Modify previous vote'),
    '#default_value' => isset($survey['modify']) ? $survey['modify'] : 1,
    '#required' => FALSE,
    '#options' => priorities_survey_modify(),
    '#description' => t('Allow or disallow the ability to modify a previous vote.')
  );
  $form['anonymous'] = array(
    '#type' => 'radios',
    '#title' => t('Anonymous voting'),
    '#default_value' => isset($survey['anonymous']) ? $survey['anonymous'] : 0,
    '#required' => FALSE,
    '#options' => priorities_survey_anonymous(),
    '#description' => t('Allow or disallow anonymous voting on this survey. Filtering by user IP ensures only one vote is allowed per user IP.')
  );
  $form['starttime'] = array(
    '#type' => 'textfield',
    '#title' => t('Start time'),
    '#default_value' => isset($survey['starttime']) ? $survey['starttime'] : 0,
    '#size' => 10,
    '#maxlength' => 10,
    '#required' => FALSE,
    '#description' => t('This is the start time (in seconds) from which the duration of the survey will be calculated. 0 = <em>never</em>. If you want to start from now, just copy the number inside the following brackets [ ') . time() . t(' ]. Alternatively you can also set a date in the past or in the future. You need to add or to substract the corresponding duration in seconds. For example, 7 days in the future from now will be: (60 * 60 * 24 * 7) + ') . time() . t(', and one month before now should be: ') . time() . t(' - (60 * 60 * 24 * 30).'),
  );
  $form['duration'] = array(
    '#type' => 'select',
    '#title' => t('Duration'),
    '#default_value' => isset($survey['duration']) ? $survey['duration'] : 0,
    '#required' => FALSE,
    '#options' => priorities_survey_duration(),
    '#description' => t('After this period in seconds from the <em>Start time</em> above, the survey will go from <em>public</em> or <em>active</em> to <em>closed</em> automatically.'),
  );
  $form['display'] = array(
    '#type' => 'radios',
    '#title' => t('Display'),
    '#default_value' => isset($survey['display']) ? $survey['display'] : 0,
    '#required' => FALSE,
    '#options' => priorities_survey_display(),
    '#description' => t('Each priority_alternative will be displayed as a full node when the display is set to <em>default</em>, while the body will be displayed inside a collapsible fieldset when the display is set to <em>collapsible</em>.'), 
  );
  $form['sorted'] = array(
    '#type' => 'radios',
    '#title' => t('Sorted results'),
    '#default_value' => $survey['sorted'],
    '#required' => FALSE,
    '#options' => array(1 => t('by score'), 0 => t('unsorted (defaults to alternatives creation time)')),
  );
  $form['alternatives'] = array(
    '#type' => 'fieldset',
    '#title' => t('Alternatives'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => FALSE,
  );
  $form['alternatives']['nodetype'] = array(
    '#type' => 'radios',
    '#title' => t('Users may link a survey to this content type'),
    '#options' => node_type_get_names(),
    '#default_value' => $survey['nodetype'],
    '#description' => t('The taxonomy term defined below will be available for nodes of this type.'),
    '#required' => TRUE,
  );
  $form['alternatives']['t_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Term name associated with all alternatives of this survey'),
    '#default_value' => $survey['t_name'],
    '#maxlength' => 255,
    '#description' => t('The name of this taxonomy term. If unset, defaults to &lt;Title&gt; Alternative'),
  );
  $form['alternatives']['t_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $survey['t_description'],
    '#description' => t('A description of the taxonomy term. To be displayed on taxonomy/term pages and RSS feeds.'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit function for the 'Delete' button on priorities_admin_form().
 *
 * @see priorities_admin_form()
 * @see priorities_admin_form_validate()
 * @see priorities_admin_form_submit()
 */
function priorities_admin_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $form_state['redirect'] = array('admin/config/priorities/delete/' . $form_state['values']['survey_name'], array('query' => $destination));
}

/**
 * Form validation handler for priorities_admin_form().
 *
 * @see priorities_admin_form()
 * @see priorities_admin_form_delete_submit()
 * @see priorities_admin_form_submit()
 */
function priorities_admin_form_validate($form, &$form_state) {
  $item = $form_state['values'];
  if ($form['#insert']) {
    if (preg_match('/[^a-z0-9-]/', $item['survey_name'])) {
      form_set_error('survey_name', t('The survey name may only consist of lowercase letters, numbers, and hyphens.'));
    }
    if (drupal_strlen($item['survey_name']) > PRIORITIES_MAX_SURVEY_NAME_LENGTH_UI) {
      form_set_error('survey_name', format_plural(PRIORITIES_MAX_SURVEY_NAME_LENGTH_UI, "The survey name can't be longer than 1 character.", "The survey name can't be longer than @count characters."));
    }
    // We will add 'priorities-' to the survey name to help avoid name-space conflicts.
    $item['survey_name'] = 'priorities-' . $item['survey_name'];
    if (db_query("SELECT survey_name FROM {priorities_surveys} WHERE survey_name = :survey_name", array(':survey_name' => $item['survey_name']))->fetchField()) {
      form_set_error('survey_name', t('The survey already exists.'));
    }
  }
}

/**
 * Form submission handler for priorities_admin_form().
 *
 * @see priorities_admin_form()
 * @see priorities_admin_form_delete_submit()
 * @see priorities_admin_form_validate()
 */
function priorities_admin_form_submit($form, &$form_state) {
  // Remove unnecessary values.
  form_state_values_clean($form_state);

  $survey = $form_state['values'];
  // Description is an array of value and format.
  $survey['format'] = $survey['description']['format'];
  $survey['description'] = $survey['description']['value'];

  // Create a new taxonomy term or update existing one.
  $vid = variable_get('priorities_vid');
  $vocabulary = taxonomy_vocabulary_load($vid);
  if (empty($vocabulary)) {
    return;
  }

  if (empty($survey['t_name'])) {
    $survey['t_name'] = $survey['title'] . ' Alternative';
  }
  $term = (object) array("vid" => $vid, "name" => $survey['t_name'], 'description' => $survey['t_description']);
  $taxonomies = taxonomy_get_term_by_name($survey['t_name']);
  if (!empty($taxonomies)) {
    $taxonomy = array_shift($taxonomies);
    $term->tid = $taxonomy->tid;
  }
  taxonomy_term_save($term);
  $tid = $term->tid;

  if ($form['#insert']) {
    // Eventually attach taxonomy field to content type.
    $field_name = 'taxonomy_' . $vocabulary->machine_name;
    $instance = field_info_instance('node', $field_name, $survey['nodetype']);
    if (empty($instance)) {
      $instance = array(
        'label' => $vocabulary->name,
        'field_name' => $field_name,
        'bundle' => $survey['nodetype'],
        'entity_type' => 'node',
        'description' => t('Choose one or more surveys for this alternative.'),
        'widget' => array(
          'type' => 'select',
        ),
        'display' => array(
          'default' => array('label' => 'hidden', 'type' => 'hidden'),
          'teaser' => array('label' => 'hidden', 'type' => 'hidden'),
          'full' => array('label' => 'hidden', 'type' => 'hidden'),
        ),
      );
      field_create_instance($instance);
    }
  
    // Add 'priorities-' to the survey to help avoid name-space conflicts.
    $survey['survey_name'] = 'priorities-' . $survey['survey_name'];

    // Store into database.
    db_insert('priorities_surveys')
      ->fields(array(
        'survey_name' => $survey['survey_name'],
        'title' => $survey['title'],
        'description' => $survey['description'],
        'format' => $survey['format'],
        'sorted' => $survey['sorted'],
        'status' => $survey['status'],
        'showbefore' => $survey['showbefore'],
        'modify' => $survey['modify'],
        'anonymous' => $survey['anonymous'],
        'starttime' => $survey['starttime'],
        'duration' => $survey['duration'],
        'display' => $survey['display'],
        'nodetype' => $survey['nodetype'],
        'tid' => $tid,
    ))
    ->execute();

    // Add the new survey to the menu.
    priorities_build_menus($survey);
  }
  else {
    $previous_survey = priorities_survey_load($survey['survey_name']);

    // If term name has changed, delete it from vocabulary.
    if ($previous_survey['tid'] != $tid) {
      taxonomy_term_delete($previous_survey['tid']);
    }

    // If content type has changed, check taxonomy fields.
    if ($previous_survey['nodetype'] != $survey['nodetype']) {
      $field_name = 'taxonomy_' . $vocabulary->machine_name;
      // Eventually delete previous taxonomy field from content type.
      $nodetype_still_needed = db_query("SELECT nodetype from {priorities_surveys} WHERE survey_name != :survey_name AND nodetype = :nodetype", array(
        ':survey_name' => $survey['survey_name'],
        ':nodetype' => $previous_survey['nodetype']));
      if (!$nodetype_still_needed) {
        $instance = array(
          'field_name' => $field_name,
          'bundle' => $previous_survey['nodetype'],
          'entity_type' => 'node',
        );
        field_delete_instance($instance);
      }
      // Eventually attach new taxonomy field to content type.
      $instance = field_info_instance('node', $field_name, $survey['nodetype']);
      if (empty($instance)) {
        $instance = array(
          'label' => $vocabulary->name,
          'field_name' => $field_name,
          'bundle' => $survey['nodetype'],
          'entity_type' => 'node',
          'description' => t('Choose one or more surveys for this alternative.'),
          'widget' => array(
            'type' => 'select',
          ),
          'display' => array(
            'default' => array('label' => 'hidden', 'type' => 'hidden'),
            'teaser' => array('label' => 'hidden', 'type' => 'hidden'),
            'full' => array('label' => 'hidden', 'type' => 'hidden'),
          ),
        );
        field_create_instance($instance);
      }
    }

    // If status has changed, rebuild menu links.
    if ($previous_survey['status'] != $survey['status']) {
      priorities_delete_menus($survey);
      priorities_build_menus($survey);
    }

    db_update('priorities_surveys')
      ->fields(array(
        'title' => $survey['title'],
        'description' => $survey['description'],
        'format' => $survey['format'],
        'sorted' => $survey['sorted'],
        'status' => $survey['status'],
        'showbefore' => $survey['showbefore'],
        'modify' => $survey['modify'],
        'anonymous' => $survey['anonymous'],
        'starttime' => $survey['starttime'],
        'duration' => $survey['duration'],
        'display' => $survey['display'],
        'nodetype' => $survey['nodetype'],
        'tid' => $tid,
      ))
      ->condition('survey_name', $survey['survey_name'])
      ->execute();
  }

  drupal_set_message(t('The survey has been saved.'));
  $form_state['redirect'] = 'admin/config/priorities';
}

/**
 * Form builder for confirming deletion a survey.
 *
 * @param $survey
 *   A survey array returned by priorities_survey_load().
 *
 * @see priorities_admin_delete_confirm_submit()
 * @ingroup forms
 */
function priorities_admin_delete_confirm($form, &$form_state, $survey) {
  if (user_access('administer priorities')) {
    $form_state['survey'] = $survey;
    return confirm_form(
      $form,
      t('Are you sure you want to delete the survey %title?',
        array('%title' => $survey['title'])),
      'admin/config/priorities/' . $survey['survey_name']
    );
  }
  return array();
}

/**
 * Form submission handler for priorities_admin_delete_confirm().
 *
 * @see priorities_admin_delete_confirm()
 */
function priorities_admin_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $survey = $form_state['survey'];
    $form_state['redirect'] = 'admin/config/priorities';
    $has_rows = (bool) db_query_range('SELECT 1 FROM {priorities_surveys} WHERE survey_name = :survey_name', array(':survey_name' => $survey['survey_name']), 0, 1)->fetchField();
    if (!$has_row) {
      return;
    }

    // Eventually delete taxonomy field from content type.
    $nodetype_still_needed = db_query("SELECT nodetype from {priorities_surveys} WHERE survey_name != :survey_name AND nodetype = :nodetype", array(
      ':survey_name' => $survey['survey_name'],
      ':nodetype' => $survey['nodetype']));
    if (!$nodetype_still_needed) {
      $vid = variable_get('priorities_vid');
      $vocabulary = taxonomy_vocabulary_load($vid);
      if (empty($vocabulary)) {
        return;
      }
      $field_name = 'taxonomy_' . $vocabulary->machine_name;
      $instance = array(
        'field_name' => $field_name,
        'bundle' => $survey['nodetype'],
        'entity_type' => 'node',
      );
      field_delete_instance($instance);
    }

    // Delete the survey in database.
    db_delete('priorities_surveys')
      ->condition('survey_name', $survey['survey_name'])
      ->execute();

    // Delete taxonomy term.
    taxonomy_term_delete($survey['tid']);

    // Delete menu entries.
    priorities_delete_menus($survey);

    $t_args = array('%title' => $survey['title']);
    drupal_set_message(t('The survey %title has been deleted.', $t_args));
    watchdog('priorities', 'Deleted survey %title.', $t_args, WATCHDOG_NOTICE);
  }
}

/**
 * Form builder for priorities module global settings.
 *
 * @see system_settings_form()
 * @ingroup forms
 */
function priorities_settings($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'priorities') . '/priorities.css');
  $form['intro'] = array(
    '#type' => 'item',
    '#markup' => t('The priorities module allows to define some surveys, defined with a taxonomy term. The following option sets for all surveys the name and color of priorities and the titles for the pages where priorities are set and where scoring results are viewed.'),
  );

  $form['priorities_results_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of the page and menu entry to see results.'),
    '#default_value' => variable_get('priorities_results_title', t('Your Priorities')),
    '#description' => t('This title will appear on the menu and on the page presenting the scoring results.'),
  );

  $form['priorities_set_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of the page and menu entry to set priorities.'),
    '#default_value' => variable_get('priorities_set_title', t('Set priorities')),
    '#description' => t('This title will appear on the menu and on the page where priorities can be set.'),
  );

  $form['priorities_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Priorities'),
    '#collapsible' => TRUE,
    '#tree' => FALSE,
    '#prefix' => '<div class="clear-block" id="priorities-choice-wrapper">',
    '#suffix' => '</div>',
  );

  $form['priorities_wrapper']['priorities_priorities'] = array(
    '#prefix' => '<div id="priorities-choices">',
    '#suffix' => '</div>',
    '#theme' => 'priorities_choices',
    '#tree' => TRUE,
  );

  $current_priorities = variable_get('priorities_priorities', priorities_default_priorities());
  $form['priorities_wrapper']['priorities_priorities'][0]['label'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#title' => t('Label of priority 0'),
    '#default_value' => $current_priorities[0]['label'],
    '#description' => t('This priority means that the user does not want to give her priority for this node.'),
  );

  $form['priorities_wrapper']['priorities_priorities'][0]['class'] = array(
    '#type' => 'hidden',
    '#value' => 'wontsay',
  );

  $form['priorities_wrapper']['priorities_priorities'][0]['color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#title' => t('Hex color code of priority 0'),
    '#default_value' => $current_priorities[0]['color'],
    '#description' => '&nbsp;',
  );

  $form['priorities_wrapper']['priorities_priorities'][1]['label'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#title' => t('Label of priority 1'),
    '#default_value' => $current_priorities[1]['label'],
    '#description' => t('This priority means that the user does not know which priority to give for this node.'),
  );

  $form['priorities_wrapper']['priorities_priorities'][1]['class'] = array(
    '#type' => 'hidden',
    '#value' => 'dontknow',
  );

  $form['priorities_wrapper']['priorities_priorities'][1]['color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#title' => t('Hex color code of priority 1'),
    '#default_value' => $current_priorities[1]['color'],
    '#description' => '&nbsp;',
  );

  $form['priorities_wrapper']['priorities_priorities'][2]['label'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#title' => t('Label of priority 2'),
    '#default_value' => $current_priorities[2]['label'],
    '#description' => t('This priority means that the user gives a weak priority for this node.'),
  );

  $form['priorities_wrapper']['priorities_priorities'][2]['class'] = array(
    '#type' => 'hidden',
    '#value' => 'notimportant',
  );

  $form['priorities_wrapper']['priorities_priorities'][2]['color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#title' => t('Hex color code of priority 2'),
    '#default_value' => $current_priorities[2]['color'],
    '#description' => '&nbsp;',
  );

  $form['priorities_wrapper']['priorities_priorities'][3]['label'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#title' => t('Label of priority 3'),
    '#default_value' => $current_priorities[3]['label'],
    '#description' => t('This priority means that the user gives a moderate priority for this node.'),
  );

  $form['priorities_wrapper']['priorities_priorities'][3]['class'] = array(
    '#type' => 'hidden',
    '#value' => 'midimportant',
  );

  $form['priorities_wrapper']['priorities_priorities'][3]['color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#title' => t('Hex color of priority 3'),
    '#default_value' => $current_priorities[3]['color'],
    '#description' => '&nbsp;',
  );

  $form['priorities_wrapper']['priorities_priorities'][4]['label'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#title' => t('Label of priority 4'),
    '#default_value' => $current_priorities[4]['label'],
    '#description' => t('This priority means that the user gives a strong priority for this node.'),
  );

  $form['priorities_wrapper']['priorities_priorities'][4]['class'] = array(
    '#type' => 'hidden',
    '#value' => 'important',
  );

  $form['priorities_wrapper']['priorities_priorities'][4]['color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#title' => t('Hex color code of priority 4'),
    '#default_value' => $current_priorities[4]['color'],
    '#description' => '&nbsp;',
  );

  $form['priorities_wrapper']['priorities_priorities'][5]['label'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#title' => t('Label of priority 5'),
    '#default_value' => $current_priorities[5]['label'],
    '#description' => t('This priority means that the user gives a very strong priority for this node.'),
  );

  $form['priorities_wrapper']['priorities_priorities'][5]['class'] = array(
    '#type' => 'hidden',
    '#value' => 'veryimportant',
  );

  $form['priorities_wrapper']['priorities_priorities'][5]['color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#title' => t('Hex color code of priority 5'),
    '#default_value' => $current_priorities[5]['color'],
    '#description' => '&nbsp;',
  );

  priorities_delete_menus();
  priorities_build_menus();
  return system_settings_form($form);
}

/**
 * Returns HTML for setting the levels of priority parameters.
 *
 * @param $variables
 *   An associative array containing:
 *   - priorities_priorities: A render element for the setting page
 *     to display the parameters for the levels of priorities
 *     as an array of two columns:
 *       - a label.
 *       - an hexadecimal color code.
 *
 * @ingroup themeable
 */
function theme_priorities_choices($variables) {
  $element = $variables['priorities_priorities'];
  $rows = array();
  $headers = array(
    t('Name'),
    t('Color'),
  );

  foreach (element_children($element) as $key) {
    // No need to print the field title every time.
    unset($element[$key]['label']['#title'], $element[$key]['color']['#title']);

    // Build the table row.
    $row = array(
      'data' => array(
        array('data' => drupal_render($element[$key]['label']), 'class' => 'priorities-label'),
        array('data' => drupal_render($element[$key]['color']), 'class' => 'priorities-color'),
      )
    );

    // Add additional attributes to the row, such as a class for this row.
    if (isset($element[$key]['#attributes'])) {
      $row = array_merge($row, $element[$key]['#attributes']);
    }
    $rows[] = $row;
  }

  $output = theme('table', array('header' => $headers, 'rows' => $rows));
  return $output;
}
