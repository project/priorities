
-- SUMMARY --

The Priorities module helps on choosing the best and prioritizing
alternatives among a set of various proposals. The scoring is based on
users' participative contribution through rating each and every
alternative with a level of priority.

This module allows users to set priorities to some sets of nodes. Each
survey applies to nodes with a specific taxonomy term. The Priorities
module comes with a new node type, Alternative, dedicated to surveys.
But you can also include in a survey nodes of any node type, provided
that the specific taxonomy term related to the survey is attached to
these nodes. Authorised users can set a priority to each node inside a
survey. The priority is chosen between a set of six levels (by
default: Very important, Important, Mid important, Not important,
Don't know and Won't say). Results are computed to sort nodes, ranking
first the node of utmost importance for voting users.

A typical usage of this module is to help in a decision making
process. For each decision, a new survey is created, generating a new
taxonomy term. Then, create a node with this taxonomy term attached
for each alternative in this survey. The Priorities module will allow
users to give a priority level to each alternative and will show
results sorted by score.

Note that it can also be used for other purposes than prioritization,
for eg. to highlight points of consensus and dissent in a domain or
among a group.

Label and color of each level of priority is configurable, as well as
title, node type and taxonomy term for each survey.

The status of each survey can be set to "public" (authorized users can
answer to the survey and see its on-going results), "active"
(authorized users can only answer the survey), "closed" (authorized
users can only see its results) or "disabled" (users cannot answer the
survey, nor see its results).


-- APPROACH --

This module uses gradual notation of each level of priority (ranging
for instance from "Very important" to "Not important at all"),
together with choices such as "I don't know" or "Don't want to say".
The levels are associated to a colour scale. Such a method was
proposed by Régnier for his "abacus" (cf.
http://hdl.handle.net/2042/17995).


-- REQUIREMENTS --

Taxonomy module.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  priorities module:

  - administer priorities

    Users in roles with the "administer priorities" permission will be
    able to create new surveys, update or delete existing surveys, and
    modify settings of Priorities module.

  - create priorities_alternative nodes

    Users in roles with the "create priorities_alternative nodes"
    permission will be able to create new alternatives of default node
    type for surveys.

  - edit own priorities_alternative nodes

    Users in roles with the "edit own priorities_alternative nodes"
    permission will be able to update alternatives of default node
    type for surveys they've created.

  - edit any priorities_alternative node

    Users in roles with the "edit any priorities_alternative node"
    permission will be able to update any alternative of default node
    type for surveys.

  - delete own priorities_alternative nodes

    Users in roles with the "delete own priorities_alternative nodes"
    permission will be able to delete alternatives of default node
    type for surveys they've created.

  - delete any priorities_alternative node

    Users in roles with the "delete any priorities_alternative node"
    permission will be able to delete any alternative of default node
    type for surveys.

  - set priorities for all surveys

    Users in roles with the "set priorities for all surveys"
    permission will be able to set priorities for all surveys.

  - get priorities for all surveys

    Users in roles with the "get priorities for all surveys"
    permission will be able to see results for all surveys.

* Customize in Administer >> Site configuration >> Priorities settings:

  - the title of pages to set priorities and get results (these titles
    will be appended to the title of the survey).

  - the name and color (in hexadecimal RGB code, ex. #ADADAD) of
    priority levels.

* Add a new survey in Administer >> Site configuration >>
  Priorities >> Add a survey:

 - Choose a machine-readble name for this survey and a
   title (this title will be prepended to the title of pages set
   previously for setting priorities or getting results).

 - Optionnaly, choose the node type associated with this survey, any
   content of this node type will require the taxonomy term described
   below to be set. Defaults to "Alternative" node type, which is
   included in the Priorities module.

 - Optionnaly, choose the name of the taxonomy term associated with
   all alternatives of this survey, any node of the above node type
   with this taxonomy term will be considered as an alternative for
   this survey. Default to the title of the survey with " Alternative"
   appended. You can also choose a description for this taxonomy term
   (this description only shows up when managing taxonomy).

* For every survey "survey title" created, configure also the following
  user permissions in Administer >> User management >> Permissions >>
  priorities module:

  - set priorities for "survey title"

    Users in roles with the "set priorities" permission will be able 
    to set priorities on the survey "survey title".

  - get priorities for "survey title"

    Users in roles with the "get priorities" permission will be able 
    to see results of survey "survey title".

  Note that you have to configure these permissions EACH TIME YOU
  CREATE A NEW SURVEY.

  You can avoid the configuration of permissions each time you create
  a survey, by setting "set/get priorities for all surveys"
  permissions (see above), which take precedence over "set/get
  priorities for 'survey title'" permissions.

-- USAGE --

For any configured survey "survey_name":

 - authorized users can set priorities in priorities/survey_name/set 

 - authorized users can get results in priorities/survey_name/results


-- EXAMPLE --

A comprehensive example, with screenshots and description of steps to perform,
on how to use Priorities modules for prioritizing some webdesign tasks
is detailed on
http://www.sopinspace.com/products/drupal-priorities-module#Example.


-- INTEGRATION WITH ORGANIC GROUPS --

Priorities module can perfectly works with organic groups:

* Alternative content type should be configured as a post in a group.
* Access rights for og are fulfilled.
* A survey is displayed (to be answered, or to see its results) to a
  specific user if and only if there is only one alternative in this
  survey which this user has access to.
* These properties allow to propose different alternatives for the
  same surveys, according to group(s) the user belongs to.

* A list of surveys accessible from a group can simply be retrieved
  and displayed with:

    print theme('priorities_surveys', priorities_get_surveys($gid));

 with $gid being the group identifier.


-- CONTACT --

Current maintainer:
* Gérald SÉDRATI-DINET - http://drupal.org/user/616006

This project has been sponsored by:
* SOPINSPACE
  One of the European reference solution providers for participatory democracy 
  and public debate using the Internet. Integrator of agile solutions 
  for Web-based collaborative work, including user support. Visit 
  http://www.sopinspace.com for more information.
